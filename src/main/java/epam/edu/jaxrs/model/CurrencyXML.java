package epam.edu.jaxrs.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "currency")
@XmlAccessorType(XmlAccessType.FIELD)
public class CurrencyXML {

	@XmlElement(name = "date")
	private Date date;

	@XmlElement(name = "digital_code")
	private int digitalCode;

	@XmlElement(name = "letter_code")
	private LetterCode letterCode;

	@XmlElement(name = "number_of_units")
	private BigDecimal numberOfUnits;

	@XmlElement(name = "currency_name")
	private String currencyName;

	@XmlElement(name = "exchange_rate")
	private BigDecimal exchangeRate;

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public BigDecimal getNumberOfUnits() {
		return numberOfUnits;
	}

	public void setNumberOfUnits(BigDecimal numberOfUnits) {
		this.numberOfUnits = numberOfUnits;
	}

	public BigDecimal getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public int getDigitalCode() {
		return digitalCode;
	}

	public void setDigitalCode(int digitalCode) {
		this.digitalCode = digitalCode;
	}

	public BigDecimal getAmount() {
		return numberOfUnits;
	}

	public void setAmount(BigDecimal amount) {
		this.numberOfUnits = amount;
	}

	public String getCurrencyName() {
		return currencyName;
	}

	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}

	public BigDecimal getRate() {
		return exchangeRate;
	}

	public void setRate(BigDecimal rate) {
		this.exchangeRate = rate;
	}

	public LetterCode getLetterCode() {
		return letterCode;
	}

	public void setLetterCode(LetterCode letterCode) {
		this.letterCode = letterCode;
	}
}
