package epam.edu.jaxrs.model;

public enum LetterCode {
	USD, EUR, UAH, AUD, GBP, DKK, CAD, HUF, AZN, BYR, ISK, KZT, MDL, NOK, PLN, RUB, SGD, XDR, TRY, TMT, UZS, CZK, SEK, CHF, CNY, JPY;

	  public static boolean contains(LetterCode letterCode)
	  {
	      for(LetterCode code : values())
	           if (code.name().equals(letterCode)) 
	              return true;
	      return false;
	

	};
}

