package epam.edu.jaxrs.model;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "currencies")
@XmlAccessorType(XmlAccessType.FIELD)
public class CurrenciesResponse {

	@XmlElement(name = "currency")
	private List<CurrencyXML> currencies = null;

	public List<CurrencyXML> getCurrencies() {
		return currencies;
	}

	public void setCurrencies(List<CurrencyXML> currencies) {
		this.currencies = currencies;
	}

}
