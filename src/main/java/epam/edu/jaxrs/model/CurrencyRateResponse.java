package epam.edu.jaxrs.model;

import java.math.BigDecimal;

public class CurrencyRateResponse {

	private LetterCode baseCurrency;
	private LetterCode literalCode;
	private String currencyName;
	private BigDecimal numberOfUnits;
	private BigDecimal exchangeRate;

	public LetterCode getBaseCurrency() {
		return baseCurrency;
	}

	public void setBaseCurrency(LetterCode baseCurrency) {
		this.baseCurrency = baseCurrency;
	}

	public LetterCode getLiteralCode() {
		return literalCode;
	}

	public void setLiteralCode(LetterCode literalCode) {
		this.literalCode = literalCode;
	}

	public BigDecimal getNumberOfUnits() {
		return numberOfUnits;
	}

	public void setNumberOfUnits(BigDecimal numberOfUnits) {
		this.numberOfUnits = numberOfUnits;
	}

	public String getCurrencyName() {
		return currencyName;
	}

	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}

	public BigDecimal getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	@Override
	public String toString() {
		return "CurrencyRateResponse [baseCurrency=" + baseCurrency + ", literalCode=" + literalCode + ", currencyName=" + currencyName + ", numberOfUnits=" + numberOfUnits + ", exchangeRate="
				+ exchangeRate + "]";
	}
}
