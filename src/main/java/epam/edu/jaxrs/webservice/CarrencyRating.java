package epam.edu.jaxrs.webservice;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import epam.edu.jaxrs.model.CurrencyRateResponse;
import epam.edu.jaxrs.model.CurrencyXML;
import epam.edu.jaxrs.model.LetterCode;
import epam.edu.service.impl.CurrencyXmlParserImpl;

@Path("/currencies")
public class CarrencyRating {
	private static List<CurrencyXML> currenciesXML;
	private static final MathContext mc = new MathContext(2, RoundingMode.HALF_EVEN);

	@GET
	@Path("/rate")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public String sayXMLHello(@DefaultValue("UAH") @QueryParam("baseCurrency") LetterCode baseCurrency, @QueryParam("currencyToRate") List<LetterCode> currenciesToRate) {
		currenciesXML = CurrencyXmlParserImpl.getAllCurrenciesRate();
		if (currenciesToRate.isEmpty())
			currenciesToRate = Arrays.asList(LetterCode.values());
		List<CurrencyRateResponse> response = generateCurrencyRateResponse(baseCurrency, currenciesXML, currenciesToRate);
		return response.toString();
	}

	private List<CurrencyRateResponse> generateCurrencyRateResponse(LetterCode baseCurrencyCode, List<CurrencyXML> currenciesXML, List<LetterCode> currenciesToRate) {
		CurrencyXML baseCurrency = getBaseCurrency(baseCurrencyCode);
		List<CurrencyRateResponse> listResponse = new ArrayList<CurrencyRateResponse>();
		for (CurrencyXML currency : currenciesXML) {
			if (currenciesToRate.contains(currency.getLetterCode())) {
				CurrencyRateResponse currencyToResponse = createCurrencyRateResponse(baseCurrency, currency);
				listResponse.add(currencyToResponse);
			}
		}
		return listResponse;
	}

	private CurrencyXML getBaseCurrency(LetterCode baseCurrencyCode) {
		for (CurrencyXML currency : currenciesXML) {
			if (currency.getLetterCode() == baseCurrencyCode)
				return currency;
		}
		return null;
	}

	private CurrencyRateResponse createCurrencyRateResponse(CurrencyXML baseCurrency, CurrencyXML currency) {
		CurrencyRateResponse currencyForResponse = new CurrencyRateResponse();
		currencyForResponse.setCurrencyName(currency.getCurrencyName());
		currencyForResponse.setNumberOfUnits(currency.getNumberOfUnits());
		currencyForResponse.setLiteralCode(currency.getLetterCode());
		if (baseCurrency == null) {
			currencyForResponse.setExchangeRate(currency.getExchangeRate());
			currencyForResponse.setBaseCurrency(LetterCode.UAH);
		} else {
			BigDecimal priceForOneBaseCurrencyUnit = baseCurrency.getExchangeRate().divide(baseCurrency.getAmount(), mc);
			BigDecimal priceForOneCurrencyToRateUnit = currency.getExchangeRate().divide(currency.getAmount(), mc);
			BigDecimal exchangeRate = priceForOneBaseCurrencyUnit.divide(priceForOneCurrencyToRateUnit, mc);
			currencyForResponse.setExchangeRate(exchangeRate.multiply(new BigDecimal(100)));
			currencyForResponse.setNumberOfUnits(new BigDecimal(100));
			currencyForResponse.setBaseCurrency(baseCurrency.getLetterCode());
		}
		return currencyForResponse;
	}

}
