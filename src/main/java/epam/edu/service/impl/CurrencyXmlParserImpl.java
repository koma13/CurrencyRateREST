package epam.edu.service.impl;

import java.io.File;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import epam.edu.jaxrs.model.CurrenciesXML;
import epam.edu.jaxrs.model.CurrencyXML;

public class CurrencyXmlParserImpl {

	private static final String CURRENCY_CONVERTION_RATE_FILE = "search_results.xml";

	public static List<CurrencyXML> getAllCurrenciesRate(){
		JAXBContext jaxbContext;
		CurrenciesXML currencies = new CurrenciesXML();
		try {
			jaxbContext = JAXBContext.newInstance(CurrenciesXML.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			currencies = (CurrenciesXML) jaxbUnmarshaller.unmarshal(new File(CURRENCY_CONVERTION_RATE_FILE));
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return currencies.getCurrencies();
	}
}